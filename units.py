#Unit creation

import ctypes
import sdl2.sdlimage
from sdl2 import *
from random import randint

class Unit:
    def __init__(self, posx = 0, posy = 0, sprth = 10, sprtw = 32):
        self.vel = [0, 0] 
        self.rect = SDL_Rect(posx, posy, sprtw, sprth)
        self.hits = 100
        self.damage = 5
        #Used for wiggly pattern
        self.count = 2
        self.ydir = -1
        self.position = [posx, posy]
        self.pattern = ""
        self.score = 0
        #Used for linear thingies
        self.dest = [0, 0]

#Simple collision that moves unit a bit to opposite direction
def collide(target, damage):
    if target.vel[0] != 0:
        target.rect.x = target.rect.x + target.vel[0] * -5
    if target.vel[1] != 0:
        target.rect.y = target.rect.y + target.vel[1] * -5
    target.hits -= damage

#Used with missiles
def hit(target):
    target.hits = target.hits -50

#Creates one enemy to random position
def CreateEnemy(posx, posy, destx, desty, pattern):
    enemy = Unit(posx, posy, 20)
    enemy.dest = [destx, desty]
    enemy.pattern = pattern
    enemy.sprite = "tmpenemy"
    enemy.score = 40
    return enemy
 
#Initial settings for units in a wave, ypos is starting point of wave
def CreateWiggly(ypos):
    enemy = Unit(800, ypos, 20)
    enemy.pattern = "wiggly"
    enemy.sprite = "tmpenemy"
    enemy.score = 50
    return enemy

def CreatePlanetary(posx, posy, velx, vely, hits,  sprite, count = -1, sprtw = 64, sprth = 64):
    planetary = Unit(posx, posy, sprth, sprtw)
    planetary.vel = [velx, vely]
    planetary.damage = 1000
    planetary.pattern = "planetary"
    planetary.hits = hits
    planetary.count = count
    planetary.sprite = "tempplanet"
    print("this fucking thing")
    return planetary
