def move(target):
    target.rect.x += target.vel[0] 
    target.rect.y += target.vel[1]
    return target.rect
