from sdl2 import *

#Texture creation function to make it less painful
def loadTexture(image, renderer):
    texture = SDL_CreateTextureFromSurface(renderer, image)
    SDL_FreeSurface(image)
    return texture


