#Not animation atm, just a class to contain the data
import sys
import ctypes
import sdl2.sdlimage
from sdl2 import *
from sdl2.sdlmixer import *

class Explosion:
    def __init__(self, posx = 0, posy = 0):
        self.rect = SDL_Rect(posx, posy, 32, 32)
        self.sheet = sdl2.sdlimage.IMG_Load(b"assets/explosion.png")
        self.audio = Mix_LoadWAV(b"assets/explosion.ogg")
        self.sheetpart = SDL_Rect(0, 0, 32, 32)
        self.frame = 0

class Background:
    def __init__(self, posx = 0, posy = 0, sheet = ""):
        self.rect = SDL_Rect(posx, posy, 800, 600)
        self. sheet = sheet
        self.sheetpart = SDL_Rect(0, 0, 800, 600)
        self.frame = 0
        self.tock = 0
        self.speed = 0 #intention to use speed as modulo so higher value means slower scrolling

def Blow(target):
    boom = Explosion(target.rect.x, target.rect.y)
    return boom

def ExpAnima(target):
    target.frame += 1
    target.sheetpart.x = target.frame*32
    #if frame == 6:
    #    return -1

def CreateBackground(posx, posy, speed, sheet = ""):
    bg = Background(posx, posy, sheet)
    bg.speed = speed
    bg.tock = speed
    return bg

def BackgroundAnima(target):
    if target.tock % target.speed == 0:
        target.frame += 1
        if target.frame == 800:
            target.frame = 0
    target.sheetpart.x = target.frame
