#Projectile creation

import sys
import ctypes
import sdl2.sdlimage
from sdl2 import *
from sdl2.sdlmixer import *

#Class to contain projectile information
class Projectile:
    def __init__(self,posx = 0, posy = 0):
        self.vel = [0, 0]
        self.rect = SDL_Rect(posx, posy, 10, 10)
        self.sheet = sdl2.sdlimage.IMG_Load(b"assets/tempmissiles.png")
        self.audio = Mix_LoadWAV(b"assets/missile.ogg")
        self.hits = 1
        self.textrect = SDL_Rect(0, 0, 10, 10)
        self.time = 0
        self.damage = 0

#Dirty projectile spawning function that sets the projectile velocity
#and selects the appropriate sprite from sprite sheet
def Shoot(direction, posx, posy, time):
    missile = Projectile(posx, posy)
    missile.time = time

    if direction[0] > -1 and direction[1] == 0:
        missile.rect.x, missile.rect.y = posx+32, posy
        missile.textrect.x = 40
        missile.vel = [3, 0]
    if direction[0] < 0 and direction[1] == 0:
        missile.rect.x, missile.rect.y = posx, posy + 10
        missile.textrect.x = 30
        missile.vel = [-3, 0]
    if direction[0] > 0 and direction[1] > 0:
        missile.rect.x, missile.rect.y = posx+32, posy + 10
        missile.textrect.x = 50
        missile.vel = [3, 3]
    if direction[0] < 0 and direction[1] > 0:
        missile.rectx, missile.rect.y = posx, posy
        missile.textrect.x = 70
        missile.vel = [-3, 3]
    if direction[0] > 0 and direction[1] < 0:
        missile.rect.x, missile.rect.y = posx+32, posy
        missile.textrect.x = 20
        missile.vel = [3, -3]
    if direction[0] < 0 and direction[1] < 0:
        missile.rect.x, missile.rect.y = posx, posy
        missile.vel = [-3, -3]
    if direction[0] == 0 and direction[1] > 0:
        missile.rect.x, missile.rect.y = posx+11, posy+10
        missile.textrect.x = 60
        missile.vel = [0, 3]
    if direction[0] == 0 and direction[1] < 0:
        missile.rect.x, missile.rect.y = posx+11, posy
        missile.textrect.x = 10
        missile.vel = [0, -3]
    return missile

def ELinearShoot(shooter):
    missile = Projectile((shooter.rect.x - 10), shooter.rect.y)
    missile.textrect.x = 30
    missile.vel = [-2, 0]
    missile.damage = 20
    return missile

