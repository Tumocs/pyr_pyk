#This is to track some keystates

class KeyState:
    def __init__(self):
        self.pressed = False

KeyW = KeyState()
KeyA = KeyState()
KeyS = KeyState()
KeyD = KeyState()
KeyUP = KeyState()
KeyDOWN = KeyState()
KeyLEFT = KeyState()
KeyRIGHT = KeyState()
KeySPACE = KeyState()
PadRTrig = KeyState()
KeyArray = [KeyW, KeyA, KeyS, KeyD, KeyUP, KeyDOWN, KeyLEFT, KeyRIGHT,KeySPACE, PadRTrig]
