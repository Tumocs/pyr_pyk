import ctypes
import sdl2
from sdl2 import *
import sdl2.sdlimage
from sdl2.sdlttf import *
from texture import *

#CLASS to contain all the necessary menu information
class menupage:
    def __init__(self, entry1s = b"", entry2s = b"", entry3s = b"", entry4s = b"", entry5s = b""):
        #Menu entry positions, first is used as a header
        self.entry1 = SDL_Rect(250, 40, 300, 100)
        self.entry2 = SDL_Rect(250, 180, 300, 100)
        self.entry3 = SDL_Rect(138, 320, 525, 100)
        self.entry4 = SDL_Rect(250, 460, 300, 100)
        self.entry5 = SDL_Rect(600, 500, 100, 75)
        #Separately declared entry, used in settings with digits
        self.modified1 = SDL_Rect(0, 0, 0, 0)
        #Entry names, need to be ctype strings
        self.entry1s = entry1s 
        self.entry2s = entry2s
        self.entry3s = entry3s
        self.entry4s = entry4s
        self.entry5s = entry5s

#Menu showing function
def menu(renderer, event, controller, volvalue, controllers):

    controllername = []
    for a in controllers:
        controllername.append(b"{} + {}".format(a+1, SDL_GameControllerMapping(controllers[a])))
    if len(controllername) == 0:
        controllername = [b"NO", b"CONTROLLERS", b"DETECTED"]
    #Declaring different menus
    mainmenu = menupage(b"MENU", b"PLAY", b"OPTIONS", b"QUIT")
    optionmenu = menupage(b"OPTIONS", b"CONTROLLERS", b"VOLUME", b"", b"APPLY")
    optionmenu.entry2.x = 100
    optionmenu.entry3.x = 100
    optionmenu.entry3.w = 400
    optionmenu.modified1 = SDL_Rect(600, 320, 100, 100)
    controllermenu = menupage(b"CONTROLLERS", b"%s" % controllername[0], b"%s" \
            % controllername[1], b"%s" %controllername[2], b"APPLY")
    controllermenu.entry2.x = 100
    controllermenu.entry3.x = 100
    controllermenu.entry4.x = 100

    #Initializing fonts and whatnot
    Sans = TTF_OpenFont(b"assets/xolonium.ttf", 25)
    white = SDL_Color(255,255,255)
    arrowimage = sdl2.sdlimage.IMG_Load(b"assets/tmparrow.png")
    arrowtexture = loadTexture(arrowimage, renderer)

    loadlevel = "levels/level1"
    mod1 = 0
    mod2 = 0

    currentmenu = mainmenu
    arrowpos = 0

    menu = True
    #Menu loop
    while menu:
        #Entry lists and dynamic entries, dunno if creating these textures constantly kill memory (yes they do)
        entries = [currentmenu.entry2, currentmenu.entry3, currentmenu.entry4, currentmenu.entry5]
        entrystrings = [currentmenu.entry2s, currentmenu.entry3s, currentmenu.entry4s, currentmenu.entry5s]
        maxarrowpos = len(entries) - 1       
        entry1 = TTF_RenderText_Solid(Sans, b"%s" % currentmenu.entry1s, white)
        entry2 = TTF_RenderText_Solid(Sans, b"%s" % currentmenu.entry2s, white)
        entry3 = TTF_RenderText_Solid(Sans, b"%s" % currentmenu.entry3s, white)
        entry4 = TTF_RenderText_Solid(Sans, b"%s" % currentmenu.entry4s, white)
        entry5 = TTF_RenderText_Solid(Sans, b"%s" % currentmenu.entry5s, white)
        modified1 = TTF_RenderText_Solid(Sans, b"%d" % mod1, white)
        entry1texture = loadTexture(entry1, renderer)
        entry2texture = loadTexture(entry2, renderer)
        entry3texture = loadTexture(entry3, renderer)
        entry4texture = loadTexture(entry4, renderer)
        entry5texture = loadTexture(entry5, renderer)
        modified1texture = loadTexture(modified1, renderer)
        time = SDL_GetTicks()

        #Dirty input handling once again controller in menus not ready
        while SDL_PollEvent(ctypes.byref(event)) != 0:
            if event.type == SDL_QUIT:
                return SDL_QUIT
                break
            if (SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_LEFTY) < -1000) or \
                    (SDL_GameControllerGetButton(controller, SDL_CONTROLLER_BUTTON_DPAD_DOWN) == 1):
                arrowpos -= 1
                if arrowpos < 0:
                    arrowpos = len(entries) -1
            if (SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_LEFTY) > 1000) or \
                    (SDL_GameControllerGetButton(controller, SDL_CONTROLLER_BUTTON_DPAD_UP) == 1):
                arrowpos += 1
                if arrowpos > maxarrowpos:
                    arrowpos = 0
            if SDL_GameControllerGetButton(controller, SDL_CONTROLLER_BUTTON_A) == 1:
                select = True
            if SDL_GameControllerGetButton(controller, SDL_CONTROLLER_BUTTON_A) == 0:
                select = False
            if SDL_GameControllerGetButton(controller, SDL_CONTROLLER_BUTTON_START) == 1:
                    return 0
                    break
            if event.type == SDL_KEYDOWN:
                if event.key.keysym.sym == sdl2.SDLK_ESCAPE:
                    if currentmenu == mainmenu:
                        return 0
                        break
                    if currentmenu != mainmenu:
                        currentmenu = mainmenu
                if event.key.keysym.sym == sdl2.SDLK_UP:
                    arrowpos -= 1
                    if len(entrystrings[arrowpos]) == 0:
                        arrowpos -= 1
                if arrowpos < 0:
                    arrowpos = len(entries) - 1
                if event.key.keysym.sym == sdl2.SDLK_DOWN:
                    arrowpos += 1
                if arrowpos > maxarrowpos:
                    arrowpos = 0
                if event.key.keysym.sym == sdl2.SDLK_RETURN:
                    select = True
                #In case of VOLUME change volume value with left and right buttons
                if event.key.keysym.sym == sdl2.SDLK_LEFT and entrystrings[arrowpos] == b'VOLUME':
                    mod1 -= 5
                    if mod1 < 0:
                        mod1 = 0
                if event.key.keysym.sym == sdl2.SDLK_RIGHT and entrystrings[arrowpos] == b'VOLUME':
                    mod1 += 5
                    if mod1 > 100:
                        mod1 = 100

            if event.type == SDL_KEYUP:
                if event.key.keysym.sym == sdl2.SDLK_RETURN:
                    select = False
            if len(entrystrings[arrowpos]) == 0:
                arrowpos += 1
                if arrowpos > maxarrowpos:
                    arrowpos = 0

            #Compare current entry string and act accordingly
            while select:
                if entrystrings[arrowpos] == b'PLAY':
                    menu = False
                    SDL_DestroyTexture(entry1texture)
                    SDL_DestroyTexture(entry2texture)
                    SDL_DestroyTexture(entry3texture)
                    SDL_DestroyTexture(entry4texture)
                    SDL_DestroyTexture(entry5texture)
                    SDL_DestroyTexture(arrowtexture)
                    return loadlevel
                if entrystrings[arrowpos] == b'OPTIONS':
                    currentmenu = optionmenu
                    mod1 = volvalue
                    break
                if entrystrings[arrowpos] == b'QUIT':
                    return SDL_QUIT
                if entrystrings[arrowpos] == b'CONTROLLERS':
                    currentmenu = controllermenu
                    break
                if currentmenu == controllermenu:
                    if len(controllers)!=0 and entrystrings[arrowpos] != b'APPLY':
                        mod2 = arrowpos-1
                        break
                    else:
                        break
                if entrystrings[arrowpos] == b'APPLY':
                    return "options/{}/{}".format(mod1,mod2)
                select = False

        #Render all the things
        SDL_RenderClear(renderer)
        arrowrect = SDL_Rect(entries[arrowpos].x - 32, int(entries[arrowpos].y+entries[arrowpos].h / 2 - 16), 32, 32) 
        SDL_RenderCopy(renderer, entry1texture, None, currentmenu.entry1)
        SDL_RenderCopy(renderer, entry2texture, None, currentmenu.entry2)
        SDL_RenderCopy(renderer, entry3texture, None, currentmenu.entry3)
        SDL_RenderCopy(renderer, entry4texture, None, currentmenu.entry4)
        SDL_RenderCopy(renderer, entry5texture, None, currentmenu.entry5)
        SDL_RenderCopy(renderer, modified1texture, None, currentmenu.modified1)
        SDL_RenderCopy(renderer, arrowtexture, None, arrowrect)
        SDL_RenderPresent(renderer)
        SDL_Delay(10)
        #Destroy textures to prevent all the shits from filling up memory
        SDL_DestroyTexture(entry1texture)
        SDL_DestroyTexture(entry2texture)
        SDL_DestroyTexture(entry3texture)
        SDL_DestroyTexture(entry4texture)
        SDL_DestroyTexture(entry5texture)
        SDL_DestroyTexture(modified1texture)



