#! /usr/bin/python3

import sys
import ctypes
import sdl2.sdlimage
import sdl2.sdlttf
import units
import projectile
import pattern
import random
import animation
import keystate
import commands
import texture
import menu
from sdl2 import *
from sdl2.sdlmixer import *
from random import randint

parsed = []

enemies = []
missiles = []
enemy_projectiles = []
explosions = []
KeyArray = []
prodir = []
loadlevel = ""
levelline = []
backgrounds = []
Shoot = projectile.Shoot
Unit = units.Unit
collide = units.collide
hit = units.hit
controllers = []
CreateEnemy = units.CreateEnemy
CreatePlanetary = units.CreatePlanetary
loadTexture = texture.loadTexture
audio_enabled = False

#Main loop
def run():
    random.seed()

    #SDL window creation
    SDL_Init(SDL_INIT_VIDEO)
    if SDL_Init(SDL_INIT_VIDEO) != 0:
        print("SDL_Init Error: " + SDL_GetError())
        return 1
    window = SDL_CreateWindow(b"Pyr Pyk",
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_SHOWN)
    if window == None:
        print(SDL_GetError())
        SDL_Quit()
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED or SDL_RENDERER_PRESETVSYNC)
    windowsurface = SDL_GetWindowSurface(window)

    #Initialize text thingies
    sdl2.sdlttf.TTF_Init()
    Sans = sdl2.sdlttf.TTF_OpenFont(b"assets/xolonium.ttf", 25)
    white = SDL_Color(255,255,255)

    #Initialize audio
    rv = Mix_OpenAudio(48000, MIX_DEFAULT_FORMAT, 2, 1024)
    volumestep = MIX_MAX_VOLUME / 100
    volvalue = 60
    if rv != 0:
        audio_enabled = False
    else:
        audio_enabled = True

    #Image loading and texture creation
    image = sdl2.sdlimage.IMG_Load(b"assets/tempmodel.png")
    enimage = sdl2.sdlimage.IMG_Load(b"assets/tempenemy.png")
    planet = sdl2.sdlimage.IMG_Load(b"assets/tempplanet.png")
    bg1i = sdl2.sdlimage.IMG_Load(b"assets/bg1ext.png")
    bg2i = sdl2.sdlimage.IMG_Load(b"assets/bg2ext.png")
    bg3i = sdl2.sdlimage.IMG_Load(b"assets/bg3ext.png")

    SDL_UpdateWindowSurface(window)
    playertexture = loadTexture(image, renderer)
    etexture = loadTexture(enimage, renderer)
    ptexture = loadTexture(planet, renderer)
    missilesheet = loadTexture(sdl2.sdlimage.IMG_Load(b"assets/tempmissiles.png"), renderer)
    explodysheet = loadTexture(sdl2.sdlimage.IMG_Load(b"assets/explosion.png"), renderer)
    bg1 = loadTexture(bg1i, renderer)
    bg2 = loadTexture(bg2i, renderer)
    bg3 = loadTexture(bg3i, renderer)

    DEATHmsg = sdl2.sdlttf.TTF_RenderText_Solid(Sans, b"DEAD", white)
    DEATHtexture = loadTexture(DEATHmsg, renderer)

    ctrl = None

    #Controller business
    SDL_Init(SDL_INIT_GAMECONTROLLER)
    JSnum = 0
    while JSnum < SDL_NumJoysticks():
        if SDL_IsGameController(JSnum) == SDL_TRUE:
            print("Index {} is controller named {}".format(JSnum, SDL_GameControllerNameForIndex(JSnum)))
            ctrl = SDL_GameControllerOpen(JSnum)
            mapping = SDL_GameControllerMapping(ctrl)
            print("Controller {} mapped as {}".format(JSnum, mapping))
            controllers.append(ctrl)
            JSnum += 1
            
    if len(controllers) != 0:
        controller=controllers[0] 
    else:
        controller=None

    
    event = SDL_Event()

    preface = True
    while preface:
        escape = menu.menu(renderer, event, controller, volvalue, controllers)
        if escape == SDL_QUIT:
            quit(renderer, window)
            return 0
        if isinstance(escape, str):
            parsed = escape.split('/')
            if parsed[0].lower() == "levels":
                loadlevel = escape
                running = True
                preface = False
            if parsed[0].lower() == "options":
                volvalue = int(parsed[1])

    level = open(loadlevel, 'r')

    #(X position, Y position, Scrolling speed(higher value for lower speed), sheet)
    backgrounds.append(animation.CreateBackground(0, 0, 6, bg1))
    backgrounds.append(animation.CreateBackground(0, 0, 5, bg2))
    backgrounds.append(animation.CreateBackground(0, 0, 4, bg3))

    #Some initial declarations
    count = -1 #this is for enemy wave creation
    old_time = 0 #timer to calculate steps with SDL_GetTicks()
    current_time = 0
    prodir = [0, 0] #projectile direction
    player = Unit(300, 400) #initial starting position
    score = 0
    DEATH = 0
    tick = 0
    old_tick = -1
    safety = 0 #level loading safety bits
    #to monitor looptime
    oldlooptime = 0
    avglooptime = []


    #The Game
    while running:
        volume = volvalue*volumestep
        #For looptime monitoring
        #looptime = SDL_GetTicks()
        #avglooptime.append(looptime - oldlooptime)
        #print(sum(avglooptime) / float(len(avglooptime)))
        #oldlooptime = looptime

        #get the timer running
        current_time = SDL_GetTicks()
 
        #Lose condition
        if player.hits < 1 and DEATH == 0:
            explosions.append(animation.Blow(player))
            Mix_PlayChannel(-1, explosions[-1].audio, 0)
            player.rect = SDL_Rect(0, 0, 0, 0)
            DEATH = 1


        #Parse level file contents
        if tick != old_tick:
            print(tick)
            if tick == 0 and safety == 0: #safety prevents looping through multiple level entries on level change
                levelline = level.readline().split()
                safety = 1
            try:
                if tick > int(levelline[0]):
                    levelline = level.readline().split()
            except:
                pass
            try:
                if tick == int(levelline[0]):
                    if levelline[1] == "count":
                        count = int(levelline[2])
                        ypos = int(levelline[4])
                    if levelline[1] == "create":
                        enemies.append(CreateEnemy(int(levelline[3]), int(levelline[4]), int(levelline[5]), \
                                int(levelline[6]), levelline[2]))
                    if levelline[1] == "planetary":
                        print("fuck this shit")
                        enemies.append(CreatePlanetary(int(levelline[2]), int(levelline[3]), int(levelline[4]), \
                                int(levelline[5]), int(levelline[6]), levelline[7], int(levelline[8]), \
                                int(levelline[10])))
                    if levelline[1] == "level":
                        print("try loading next level")
                        loadlevel = "levels/{}".format(levelline[2])
                        level = open(loadlevel, 'r')
                        tick = -1
                        safety = 0
            except:
                pass
            print(levelline)


        #Poll for events, or running is there to make sure it doesn't get stuck while waiting for an event to happen
        while SDL_PollEvent(ctypes.byref(event)) != 0:


            #Start checking for some events
            if event.type == SDL_QUIT:
                running = False
                break

            if event.type == SDL_KEYDOWN:
                #Movement
                if event.key.keysym.sym == sdl2.SDLK_w:
                    keystate.KeyW.pressed = True
                    player.vel[1] = -2
                if event.key.keysym.sym == sdl2.SDLK_s:
                    keystate.KeyS.pressed = True
                    player.vel[1] = 2
                if event.key.keysym.sym == sdl2.SDLK_a:
                    keystate.KeyA.pressed = True
                    player.vel[0] = -2
                if event.key.keysym.sym == sdl2.SDLK_d:
                    keystate.KeyD.pressed = True
                    player.vel[0] = 2
                if event.key.keysym.sym == sdl2.SDLK_SPACE:
                    keystate.KeySPACE.pressed = True
                #Projectile direction
                if event.key.keysym.sym == sdl2.SDLK_UP:
                    keystate.KeyUP.pressed = True
                    prodir[1] = -1
                if event.key.keysym.sym == sdl2.SDLK_DOWN:
                    keystate.KeyDOWN.pressed = True
                    prodir[1] = 1
                if event.key.keysym.sym == sdl2.SDLK_LEFT:
                    keystate.KeyLEFT.pressed = True
                    prodir[0] = -1
                if event.key.keysym.sym == sdl2.SDLK_RIGHT:
                    keystate.KeyRIGHT.pressed = True
                    prodir[0] = 1
                #TEMP ENEMY CREATION
                #if event.key.keysym.sym == sdl2.SDLK_c:
                #    enemies.append(CreateEnemy())
                #if event.key.keysym.sym == sdl2.SDLK_v:
                #    count = 0

                #Enter menu
                if event.key.keysym.sym == sdl2.SDLK_ESCAPE:
                    escape = menu.menu(renderer, event, controller, volvalue, controllers)
                    #Quit the game
                    if escape == SDL_QUIT:
                        running = False
                        quit(renderer, window)
                        break
                    #See if menu returns a string, if so, do things
                    if isinstance(escape, str):
                        print(escape)
                        parsed = escape.split('/')
                        if parsed[0].lower() == "levels":
                            #Reset all the things
                            level = open(escape, 'r')
                            count = -1
                            old_time = 0
                            current_time = 0
                            prodir = [0, 0] 
                            player = Unit(300, 400) 
                            score = 0
                            DEATH = 0
                            tick = -1
                            old_tick = -2
                            safety = 0
                            oldlooptime = 0
                            del enemies[:]
                            del missiles[:]
                            del enemy_projectiles[:]
                            del explosions[:]
                            del avglooptime[:]
                        if parsed[0].lower() == "options":
                            print("settings through")
                            volvalue = int(parsed[1])
                            if len(controllers) != 0:
                                controller = controllers[int(parsed[2])]
       
                        

            if SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_LEFTX) < -1000:
                    player.vel[0] = -2
            if SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_LEFTX) > 1000:
                    player.vel[0] = 2
            if SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_LEFTY) < -1000:
                    player.vel[1] = -2
            if SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_LEFTY) > 1000:
                    player.vel[1] = 2
            if SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_RIGHTX) < -800:
                prodir[0] = -1
            if SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_RIGHTX) > 800:
                prodir[0] = 1
            if SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_RIGHTY) < -800:
                prodir[1] = -1
            if SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_RIGHTY) > 800:
                prodir[1] = 1
            if SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_TRIGGERRIGHT) >800:
                keystate.PadRTrig.pressed = True
            if SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_TRIGGERRIGHT) < 800:
                keystate.PadRTrig.pressed = False

            if event.type == SDL_CONTROLLERBUTTONDOWN:
                print("whoop")

            if event.type == SDL_KEYUP:
                #Movement
                if event.key.keysym.sym == sdl2.SDLK_w:
                    keystate.KeyW.pressed = False
                if event.key.keysym.sym == sdl2.SDLK_s:
                    keystate.KeyS.pressed = False
                if event.key.keysym.sym == sdl2.SDLK_a:
                    keystate.KeyA.pressed = False
                if event.key.keysym.sym == sdl2.SDLK_d:
                    keystate.KeyD.pressed = False
                if event.key.keysym.sym == sdl2.SDLK_UP:
                    keystate.KeyUP.pressed = False
                if event.key.keysym.sym == sdl2.SDLK_DOWN:
                    keystate.KeyDOWN.pressed = False
                if event.key.keysym.sym == sdl2.SDLK_LEFT:
                    keystate.KeyLEFT.pressed = False
                if event.key.keysym.sym == sdl2.SDLK_RIGHT:
                    keystate.KeyRIGHT.pressed = False
                if event.key.keysym.sym == sdl2.SDLK_SPACE:
                    keystate.KeySPACE.pressed = False



            #Nicer way to unset movement velocity
            if keystate.KeyW.pressed == False and keystate.KeyS.pressed == False \
                    and -900 < SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_LEFTY) < 900:
                player.vel[1] = 0
            if keystate.KeyA.pressed == False and keystate.KeyD.pressed == False \
                    and -900 < SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_LEFTX) < 900:
                player.vel[0] = 0

            #Unset projectile direction
            if keystate.KeyUP.pressed == False and keystate.KeyDOWN.pressed == False \
                    and -800 < SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_RIGHTY) < 800:
                prodir[1] = 0
            if keystate.KeyLEFT.pressed == False and keystate.KeyRIGHT.pressed == False \
                    and -800 < SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_RIGHTX) < 800:
                prodir[0] = 0
            
        #check borders
        if player.rect.x < 1 and player.vel[0] < 0:
            player.vel[0] = 0
        if player.rect.x > 767 and player.vel[0] > 0:
            player.vel[0] = 0
        if player.rect.y < 1 and player.vel[1] < 0:
            player.vel[1] = 0
        if player.rect.y > 589 and player.vel[1] > 0:
            player.vel[1] = 0

        #SHOOT
        if (keystate.KeySPACE.pressed or keystate.PadRTrig.pressed) and player.hits >0 and len(missiles) < 20:
            #Either use keystate checking or limit autofire with (current_time - missile[-1].time) > 100
            if len(missiles) == 0 or (current_time - missiles[-1].time) > 100:
                missiles.append(Shoot(prodir, player.rect.x, player.rect.y, current_time))
                if audio_enabled:
                    Mix_VolumeChunk(missiles[-1].audio, int(volume / 2))
                    Mix_PlayChannel(-1, missiles[-1].audio, 0)

        #Enemy wave counter
        if count != -1 and (tick != old_tick):
            enemies.append(units.CreateWiggly(ypos))
            count += 1
            print(count)
            if count == 6:
                count = -1

        #Move Player
        commands.move(player)

        #Scrap Missiles
        for missile in missiles:
            if missile.rect.x < 0 or missile.rect.x > 800 \
                    or missile.rect.y < 0 or missile.rect.y > 600:
                Mix_FreeChunk(missile.audio)
                missiles.remove(missile)
                # print("Missile {} scrapped".format(e))
                break

        for proj in enemy_projectiles:
            if proj.rect.x < 0 or proj.rect.x > 800 \
                    or proj.rect.y < 0 or proj.rect.y > 600:
                Mix_FreeChunk(proj.audio)
                enemy_projectiles.remove(proj)
                # print("Projectile {} scrapped".format(e))
                break
            if SDL_HasIntersection(proj.rect, player.rect) == True:
                print("You've been hit")
                player.hits -= proj.damage
                Mix_FreeChunk(proj.audio)
                enemy_projectiles.remove(proj)
                break


        #Collide Enemies with things
        for enemy in enemies:
            if enemy.hits < 1:
                #Swap enemy with an explosion in it place if hp reaches 0
                explosions.append(animation.Blow(enemy))
                score += enemy.score
                enemies.remove(enemy)
                #Play explosion audio no channel 2 with 0 loops
                if audio_enabled:
                    Mix_VolumeChunk(explosions[-1].audio, int(volume))
                    Mix_PlayChannel(-1, explosions[-1].audio, 0)
                break
            #Remove colliding missiles
            for missile in missiles:
                if SDL_HasIntersection(missile.rect, enemy.rect) == True:
                        # print("Missile {} hit enemy {}".format(d, c))
                        hit(enemy)
                        Mix_FreeChunk(missile.audio)
                        missiles.remove(missile)
                        break
            #Check enemies out of bounds
            if enemy.rect.x < -32 or enemy.rect.y < -20 or enemy.rect.y > 632:
                # print("Enemy {} out of bounds".format(c))
                enemies.remove(enemy)
                break
            if SDL_HasIntersection(player.rect, enemy.rect) == True:
                # print("Collision with enemy {}, self hits {}, enemy hits {}".format(c, player.hits, enemy.hits))
                collide(player, enemy.damage)
                if enemy.hits < 200:
                    collide(enemy, player.damage)
                  
        #RENDERING
        #Score and hitbox
        hudmessage = sdl2.sdlttf.TTF_RenderText_Solid(Sans, b"HP:%d SCORE:%d" % (player.hits, score), white)
        hudbox = SDL_Rect(0, 0, 200, 50)
        hudtexture = loadTexture(hudmessage, renderer)
        select = False
  
       
        SDL_RenderClear(renderer)

        #Render Background
        for bg in backgrounds:
            bgtext = bg.sheet
            bg.tock += 1
            animation.BackgroundAnima(bg)
            SDL_RenderCopy(renderer, bgtext, bg.sheetpart, bg.rect)

        #Render player model
        SDL_RenderCopy(renderer, hudtexture, None, hudbox)
        if player.hits > 0:
            SDL_RenderCopy(renderer, playertexture, None, player.rect)
        if DEATH == 1:
            DEATHrect = SDL_Rect(randint(290, 310), randint(140, 160), 200, 200)
            SDL_RenderCopy(renderer, DEATHtexture, None, DEATHrect)
        #Render enemies
        for enemy in enemies:
            enemy.vel = pattern.getvel(enemy)
            enemy.rect = commands.move(enemy)
            if enemy.pattern == "linear" and \
                    enemy.rect.y == player.rect.y and enemy.rect.x > player.rect.x:
                enemy_projectiles.append(projectile.ELinearShoot(enemy))
            if enemy.sprite == "tmpenemy":
                objecttexture = etexture
            if enemy.sprite == "tempplanet":
                objecttexture = ptexture
            SDL_RenderCopy(renderer, objecttexture, None, enemy.rect)
        #Render missiles
        for missile in missiles:
            missile.rect = commands.move(missile)
            SDL_RenderCopy(renderer, missilesheet, missile.textrect, missile.rect)
        for proj in enemy_projectiles:
            proj.rect = commands.move(proj)
            SDL_RenderCopy(renderer, missilesheet, proj.textrect, proj.rect)
        #Render Explosions
        for explosion in explosions:
            if tick != old_tick:
                animation.ExpAnima(explosion)
            if explosion.frame == 6:
                Mix_FreeChunk(explosion.audio)
                explosions.remove(explosion)
                break
            SDL_RenderCopy(renderer, explodysheet, explosion.sheetpart, explosion.rect)
        #This triggers that all
        SDL_RenderPresent(renderer)

        #check time with 100ms steps
        old_tick = tick
        if (current_time - old_time) > 100:
            old_time = current_time
            tick += 1
 
        SDL_Delay(8)
        SDL_DestroyTexture(hudtexture)

def quit(renderer, window):
    SDL_DestroyRenderer(renderer)
    SDL_DestroyWindow(window)
    SDL_Quit()
    return 0

if __name__ == "__main__":
    sys.exit(run())

