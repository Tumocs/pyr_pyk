#Initial movement patterns while I learn some maths again...

from sdl2 import *
from random import randint
import math

#Semi linear movement to target destination
def Linear(target):
    destx = target.dest[0]
    desty = target.dest[1]
    speedy = 0
    if target.rect.x+32 > math.fabs(desty - target.rect.y):
        target.vel[0] = -2
        speedy = 1
    if target.rect.x+32 < math.fabs(desty - target.rect.y):
        target.vel[0] = -1
        speedy = 2
    if desty - target.rect.y < 0:
        target.vel[1] = -speedy
    if desty - target.rect.y > 0:
        target.vel[1] = speedy
    if desty - target.rect.y == 0:
        target.vel[1] = 0
    return target.vel

#Sine like movement without sine
def Wiggly(target):
    diry = target.vel[1]
    dirx = target.vel[0]
    if dirx == 0 and diry == 0:
        dirx, diry = -1, 2
    if target.count > 2 or target.count < 1:
        diry = 1
    if target.count < 3 and target.count > 0:
        diry = 2
    if target.rect.x < target.position[0]-15:
        target.count += 1
        target.position[0] = target.rect.x
    if target.count == 4:
        target.count = 0
        target.ydir = -target.ydir
    target.vel = [dirx, diry*target.ydir]
    return target.vel

#Possibly slow and linear movement
def Planetary(target):
    if target.count != -1:
        if target.count % 2 == 0:
            target.vel[0] = -1
        if target.count % 2 == 1:
            target.vel[0] = 0
        target.count += 1
    #    target.vel = [-1, 0]
    return target.vel

#Function that sets the movement type depending on the set pattern
def getvel(target):
    if target.pattern == "wiggly":
        return Wiggly(target)
    if target.pattern == "linear":
        return Linear(target)
    if target.pattern == "planetary":
        return Planetary(target)


